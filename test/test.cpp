#include <Arduino.h>
#include <unity.h>
#include "connection.h"
#include "updatentp.h"
#include <LITTLEFS.h>
#include "Configfile.h"
#include <Ticker.h>
#include "checkin.h"
#include "config.h"
#include "sethttpserver.h"
#include "ota.h"
// Configfile cfg("/config.cfg");
void connectTest()
{
    connect(&cfg);
}
void testsethttpServer()
{
    connect(&cfg);

    setHttp();
    // delay(60000);
}
void testUpdateNTP()
{
    connect(&cfg);
    String re = updateNTP();
    Serial.println(re);
}
void testCheckin()
{
    connect(&cfg);
    String name = checkin("http://192.168.88.21:3333/checkin", 1, 1000, &cfg);
    Serial.println("**********************************************");
    Serial.println(name);
    Serial.println("**********************************************");
}
void testOta()
{
    connect(&cfg);
    ota(&cfg,1);
}
void setup()
{

    pinMode(2, OUTPUT);
    Serial.begin(9600);
    delay(5000);
    UNITY_BEGIN();
    // RUN_TEST(Testwrfile);
    // RUN_TEST(testApMode);
    // RUN_TEST(testConnecttomaster);
    // RUN_TEST(connectTest);
    // RUN_TEST(testUpdateNTP);
    // RUN_TEST(testCheckin);
    RUN_TEST(testsethttpServer);
    RUN_TEST(testOta);
    UNITY_END();
}

void loop()
{
    digitalWrite(2, HIGH);
    delay(1000);
    digitalWrite(2, LOW);
    delay(5000);
}