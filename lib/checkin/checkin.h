#include <Arduino.h>
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include "Configfile.h"
#ifndef CHECK_H
#define CHECK_H
#define BUFFJSON 1024
String checkin(String url,int verion,long uptime,Configfile *cfg);

#endif