#include "checkin.h"

String checkin(String url, int version, long uptime, Configfile *cfg)
{
    DynamicJsonDocument dy(BUFFJSON);
    char b[BUFFJSON];
    WiFiClient client;
    int connectcount = 0;
    if (WiFi.status() != WL_CONNECTED) // รอการเชื่อมต่อ
    {
        return "no wifi";
    }
    dy["mac"] = WiFi.macAddress();
    dy["password"] = "";
    dy["ip"] = WiFi.localIP().toString();
    dy["uptime"] = uptime;
    dy["name"] = cfg->getConfig("name");
    serializeJsonPretty(dy, b, BUFFJSON);
    Serial.println(b);
    // put your main code here, to run repeatedly:
    HTTPClient http; // Declare object of class HTTPClient
                     //   String h = cfg.getConfig("checkinurl", "http://192.168.88.21:3333/checkin");
    //"http://" + hosttraget + "/checkin";
    http.begin(client, url);                            // Specify request destination
    http.addHeader("Content-Type", "application/json"); // Specify content-type header
    // http.addHeader("Authorization", "Basic VVNFUl9DTElFTlRfQVBQOnBhc3N3b3Jk");

    int httpCode = http.POST(b);       // Send the request
    String payload = http.getString(); // Get the response payload
    Serial.print(" Http Code:");
    Serial.println(httpCode); // Print HTTP return code
    if (httpCode == 200)
    {
        Serial.print(" Play load:");
        Serial.println(payload); // Print request response payload
        deserializeJson(dy, payload);
        JsonObject obj = dy.as<JsonObject>();
        String name = obj["name"].as<String>();
        cfg->addConfig("name", name);
        return name;
    }

    http.end(); // Close connection
                //   busy = false;
    // ota();
    return "not checkin";
}