#include "updatentp.h"
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);
String updateNTP()
{
    timeClient.update();
    String formattedDate = timeClient.getFormattedTime();
    Serial.println(formattedDate);
    long t  = timeClient.getEpochTime()+25200;
    int y = year(t);
    int m = month(t);
    int d = day(t);
    Serial.printf("%d %d %d\n",y,m,d);
    return String(y)+" "+String(m)+" "+String(d)+" "+formattedDate;
}