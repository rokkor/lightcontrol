#ifndef CONNECT
#define CONNECT
#include <Arduino.h>
#include "Configfile.h"

boolean connect();
boolean connect(Configfile *p);

#endif