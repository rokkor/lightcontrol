#include "ota.h"

void ota(Configfile *p, int version)
{
    WiFiClient client;
    Serial.println("OTA");
    String b = p->getConfig("otaurl", "http://192.168.88.21:2000/rest/fw/update/light/");
    String url = b + version;
    Serial.println(url);
    t_httpUpdate_return ret = ESPhttpUpdate.update(client, url);
    switch (ret)
    {
    case HTTP_UPDATE_FAILED:
        Serial.println("[update] Update failed.");
        break;
    case HTTP_UPDATE_NO_UPDATES:
    
        Serial.println("[update] Update no Update.");
        break;
    case HTTP_UPDATE_OK:
      
        Serial.println("[update] Update ok."); // may not called we reboot the ESP
        break;
    }
}