#include <Arduino.h>
#include <ArduinoJson.h>
#include <ESP8266HTTPClient.h>
// #include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <ESP8266httpUpdate.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include "Configfile.h"
#include <Ticker.h>
#include "connection.h"
#include "readlight.h"
#include "updatentp.h"
#include "checkin.h"
#include "ota.h"
void loadram();
String name = "lightcontrol";
int version = 1;
#define jsonbuffersize 1024
Configfile cfg("/config.cfg");
Ticker t;
AsyncWebServer server(80);
struct
{
  int opentime = 0;
  int checkintime = 0;
  String checkinurl;
  int otatime = 0;
} configram;
int timetoopen = 0; // เป็นเวลาที่บอกว่าจะเปิด port นานเท่าไหร่
int updatentptime = 0;
int otatime = 0;
long checkintime = 0;
long uptime = 0;
int ledstatus = 1;
void ticker()
{
  if (timetoopen > 0)
  {
    timetoopen--;
    ledstatus = !ledstatus;
    digitalWrite(2, ledstatus);
  }
  updatentptime++;
  otatime++;
  uptime++;
}
void run(int port, int time)
{
  digitalWrite(port, 1);
  timetoopen = time;
}
void checktooff(int port)
{
  if (timetoopen <= 0)
  {
    digitalWrite(port, 0);
  }
}

const char configfile_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>Config</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
    html {font-family: Arial; display: inline-block; text-align: center;}
    h2 {font-size: 3.0rem;}
    p {font-size: 3.0rem;}
    body {max-width: 600px; margin:0px auto; padding-bottom: 25px;}
    .switch {position: relative; display: inline-block; width: 120px; height: 68px} 
    .switch input {display: none}
    .slider {position: absolute; top: 0; left: 0; right: 0; bottom: 0; background-color: #ccc; border-radius: 6px}
    .slider:before {position: absolute; content: ""; height: 52px; width: 52px; left: 8px; bottom: 8px; background-color: #fff; -webkit-transition: .4s; transition: .4s; border-radius: 3px}
    input:checked+.slider {background-color: #b30000}
    input:checked+.slider:before {-webkit-transform: translateX(52px); -ms-transform: translateX(52px); transform: translateX(52px)}

#customers {
    /* font-family: 'Karla', Tahoma, Varela, Arial, Helvetica, sans-serif; */
    border-collapse: collapse;
    width: 100%%;
    /* font-size: 12px; */
}
#btn {
  border: 1px solid #777;
  background: #6e9e2d;
  color: #fff;
  font: bold 11px 'Trebuchet MS';
  padding: 4px;
  cursor: pointer;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
}
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}
#customers td,
#customers th {
    border: 1px solid #ddd;
    padding: 8px;
}


/* #customers tr:nth-child(even){background-color: #f2f2f2;} */

#customers tr:hover {
    background-color: #ddd;
}

#customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}
</style>

<script>
function deleteallconfig()
{
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/resetconfig", true); 
    xhr.send();
}
function remove(config)
{
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/removeconfig?configname="+config, true); 

     xhr.addEventListener("readystatechange", () => {
     console.log(xhr.readystate);
    if (xhr.readyState === 4 && xhr.status === 200) {
     console.log(xhr.responseText);
     location.reload();
     }
 });
    xhr.send();
}
function add()
{
  var xhr = new XMLHttpRequest();
  var input = document.getElementById('newconfigname');
  var value = document.getElementById('newvalue');
  xhr.open("GET", "/setconfig?configname="+input.value+"&value="+value.value, true); 
  xhr.addEventListener("readystatechange", () => {
     console.log(xhr.readystate);
    if (xhr.readyState === 4 && xhr.status === 200) {
     console.log(xhr.responseText);
     var o =  JSON.parse(xhr.responseText);
     var t = document.getElementById('customers');
     var row = t.insertRow();
     row.innerHTML = "<td>"+o.setconfig+"</td><td>"+o.value+"</td><td><input value="+o.value+"></td>";
     }
 });
  xhr.send();
}
function setvalue(element,configname,value) {
  console.log("Call",element);
  var xhr = new XMLHttpRequest();
  var input = document.getElementById(configname);

  xhr.open("GET", "/setconfig?configname="+configname+"&value="+input.value, true); 
  xhr.addEventListener("readystatechange", () => {
     console.log(xhr.readystate);
    if (xhr.readyState === 4 && xhr.status === 200) {
     console.log(xhr.responseText);
    var o =  JSON.parse(xhr.responseText);
  var showvalue = document.getElementById(configname+'value');  
  console.log('Showvalue',showvalue);
  console.log('O',o);
  showvalue.innerHTML = o.value
    } else if (xhr.readyState === 4) {
     console.log("could not fetch the data");
     }
        });
  xhr.send();
}
</script>
  </head><body>
 <table id="customers">
  <tr>
  <td>Config</td><td>value</td><td>Set</td><td>#</td><td>x</td>
  </tr>
  %CONFIG%
 </table>
<hr>
New Config <input id=newconfigname> <input id=newvalue> <button  id=btn onClick="add()">add </button>
<hr>
<button id=btn onClick="deleteallconfig()">Reset Config</button>

</body></html>)rawliteral";
String fillconfig(const String &var)
{

  Serial.println(var);
  if (var == "CONFIG")
  {
    DynamicJsonDocument dy = cfg.getAll();
    serializeJsonPretty(dy, Serial);
    JsonObject documentRoot = dy.as<JsonObject>();
    String tr = "";
    for (JsonPair keyValue : documentRoot)
    {
      String v = dy[keyValue.key()];
      String k = keyValue.key().c_str();
      tr += "<tr><td>" + k + "</td><td> <label id=" + k + "value>" + v + "</label> </td> <td> <input id = " + k + " value =\"" + v + "\"></td><td><button id=btn onClick=\"setvalue(this,'" + k + "','" + v + "')\">Set</button></td><td><button id=btn onClick=\"remove('" + k + "')\">Remove</button></td></tr>";
    }
    tr += "<tr><td>heap</td><td colspan=4>" + String(ESP.getFreeHeap()) + "</td></tr>";

    return tr;
  }
  return String();
}
boolean addTorun(int port, int delay, int value, int wait)
{
  digitalWrite(port, value);
  Serial.println("Set port");
  return true;
}
int getPort(String p)
{
  if (p == "D1")
  {
    return D1;
  }
  else if (p == "D2")
  {
    return D2;
  }
  else if (p == "D5")
  {
    return D5;
  }
  else if (p == "D6")
  {
    return D6;
  }
  else if (p == "D7")
  {
    return D7;
  }
  else if (p == "D8")
  {
    return D8;
  }

  return -1;
}
String makestatus()
{
  char b[jsonbuffersize];
  DynamicJsonDocument dd(jsonbuffersize);
  dd.clear();
  dd["devicetype"] = "lightcontrol";
  dd["name"] = name;
  dd["ip"] = WiFi.localIP().toString();
  dd["mac"] = WiFi.macAddress();
  dd["ssid"] = WiFi.SSID();
  dd["signal"] = WiFi.RSSI();
  dd["version"] = version;
  dd["timetoopen"] = timetoopen;
  dd["D5"] = digitalRead(D5);
  dd["freemem"] = system_get_free_heap_size();
  dd["heap"] = system_get_free_heap_size();

  serializeJson(dd, b, jsonbuffersize);
  return String(b);
}
void setHttp()
{
  server.on("/setconfigwww", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->send_P(200, "text/html", configfile_html, fillconfig); });

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
            { 
             String re =  makestatus();
              request->send(200, "application/json", re); });

  server.on("/setconfig", HTTP_GET, [](AsyncWebServerRequest *request)
            {
              String v = request->arg("configname");
              String value = request->arg("value");
              cfg.addConfig(v, value);
              loadram();
              request->send(200, "application/json", "{\"setconfig\":\"" + v + "\",\"value\":\"" + value + "\"}"); });

  //--------------------------------------------------------------------------------------------------------------------
  server.on("/run", HTTP_GET, [](AsyncWebServerRequest *request)
            { 
                Serial.println("Run");
  String p = request->arg("port");
  char b[jsonbuffersize];
  DynamicJsonDocument dy(jsonbuffersize);
  if (p.equals("test"))
  {
   
    // doc.clear();
    dy["status"] = "ok";
    dy["port"] = p;
    dy["mac"] = WiFi.macAddress();
    dy["ip"] = WiFi.localIP().toString();
    dy["name"] = name;
    dy["uptime"] = uptime;
    // dy["ntptime"] = timeClient.getFormattedTime();
    // dy["ntptimelong"] = timeClient.getEpochTime();

    serializeJson(dy, b, jsonbuffersize);
    request->send(200, "application/json", b);
    for (int i = 0; i < 40; i++)
    {
      digitalWrite(2, !digitalRead(2));
      delay(200);
    }
    return;
  }
  String v =  request->arg("value");
  String d =  request->arg("delay");
  String w =  request->arg("wait");
  Serial.println("Port: " + p + " value : " + v + " delay: " + d);
  // message = String("run port ") + String(p) + String(" value") + String(" delay ") + String(d) + " " + timeClient.getFormattedDate();
  int value = v.toInt();
  int port = getPort(p);
  if (p == NULL)
  {
    port = D5;
    v = "1";
  }
  addTorun(port, d.toInt(), v.toInt(), w.toInt());
  dy["status"] = "ok";
  dy["port"] = p;
  dy["runtime"] = d;
  dy["value"] = value;
  dy["mac"] = WiFi.macAddress();
  dy["ip"] = WiFi.localIP().toString();
  dy["uptime"] = uptime;
  dy["timetoopen"] =  timetoopen;
  serializeJson(dy, b, jsonbuffersize);
   request->send(200, "application/json", b); });
  //--------------------------------------------------------------------------------------------------------------------
  server.begin();
}
void loadram()
{
  configram.opentime = cfg.getIntConfig("opentime", 3600);
  configram.checkintime = cfg.getIntConfig("checkintime", 300);
  configram.checkinurl = cfg.getConfig("checkinurl", "http://192.168.88.21:3333/checkin");
  configram.otatime = cfg.getIntConfig("otatime", 300);
}
void setup()
{
  Serial.begin(9600);
  cfg.openFile();
  pinMode(D5, INPUT);
  pinMode(D6, OUTPUT);
  pinMode(D4, OUTPUT);
  pinMode(D3,OUTPUT);
  connect(&cfg);
  t.attach(1, ticker);
  setHttp();
  loadram();
}

void loop()
{

  if (readlight(D5))
  {
    delay(1000);

    if (readlight(D5))
    {
      int timetorun = configram.opentime;
      run(D6, timetorun);
    }
  }

  checktooff(D6);

  if (checkintime > configram.checkintime)
  {
    checkin(configram.checkinurl, version, uptime, &cfg);
    checkintime = 0;
  }
  if (otatime > configram.otatime)
  {
    ota(&cfg, version);
    otatime = 0;
  }
  digitalWrite(D3,digitalRead(D5));
  
}